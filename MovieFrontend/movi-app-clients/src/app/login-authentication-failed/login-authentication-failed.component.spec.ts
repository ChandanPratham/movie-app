import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginAuthenticationFailedComponent } from './login-authentication-failed.component';

describe('LoginAuthenticationFailedComponent', () => {
  let component: LoginAuthenticationFailedComponent;
  let fixture: ComponentFixture<LoginAuthenticationFailedComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LoginAuthenticationFailedComponent]
    });
    fixture = TestBed.createComponent(LoginAuthenticationFailedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
