import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-authentication-failed',
  templateUrl: './login-authentication-failed.component.html',
  styleUrls: ['./login-authentication-failed.component.css']
})
export class LoginAuthenticationFailedComponent {
constructor(private router: Router){}
  back(){
this.router.navigate(['/admin-login']);
  }
}
