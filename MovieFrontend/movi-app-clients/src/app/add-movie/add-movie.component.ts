import { Component } from '@angular/core';
import { AddMovieService } from './add-movie.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.css']
})
export class AddMovieComponent {
constructor(private addMovieService: AddMovieService, private router: Router){}
  movieAdmin: boolean = true;
  enterMovie: boolean = false;
  editMovie: boolean = false;
  movieAddSuccess: boolean = false;
  movieAddFailed: boolean = false;
  ngOnInit(){
    this.movieAdmin = true;
    this.enterMovie = false;
    this.editMovie = false;
    this.movieAddSuccess = false;
    this.movieAddFailed = false;
  }
  addMovie(){
    this.movieAdmin = false;
    this.enterMovie = true;
  }


  modifyMovie(){
    this.movieAdmin = false;
    this.editMovie = true;
  }

  addMovies(submitform:any){
    console.log(submitform);
    this.addMovieService.addMovie(submitform)
    .subscribe((response:any)=>{
      const resp = response;
      console.log(resp);
      if(resp.length == 0){       
        this.movieAddFailed = true;
        this.enterMovie = false;
      }else{      
       this.movieAddSuccess = true;
       this.enterMovie = false;

      }
    }
    )
  }
  home(){
    this.ngOnInit();
  }
}
