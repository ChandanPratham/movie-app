import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class AddMovieService{
  mySubscription: any;
  constructor(private httpClient: HttpClient){}
   

addMovie(formData:any){
   
  return this.httpClient.post('/movies/AddMovie', formData);
}
}
