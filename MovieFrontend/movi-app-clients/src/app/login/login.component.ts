import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
constructor(private router: Router){}

  loginSubmit(loginForm: any){
    console.log(loginForm);
    const userId = loginForm.userId;
    const password = loginForm.password;
    if(userId === 'stella@gmail.com' && password === 'steller'){
      console.log("login");
      this.router.navigate(['/add-movie']);
    }else{
console.log("credential fail");
this.router.navigate(['/login-auth-failed']);
    }
  }
}
