import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginAuthenticationFailedComponent } from './login-authentication-failed/login-authentication-failed.component';
import { HomeComponent } from './home/home.component';
import { SearchPageComponent } from './search-page/search-page.component';
import { AddMovieComponent } from './add-movie/add-movie.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'admin-login',
    component: LoginComponent,
  },
  {
    path:'login-auth-failed',
    component: LoginAuthenticationFailedComponent
  },
  {
    path:'search/:id',
    component: SearchPageComponent
  },
  {
    path:'add-movie',
    component: AddMovieComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
