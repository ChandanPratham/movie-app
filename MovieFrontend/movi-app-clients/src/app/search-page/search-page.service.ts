import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

@Injectable({
    providedIn: 'root'
})
export class SearchPageService{
  mySubscription: any;
  constructor(private httpClient: HttpClient){}

    ngOnInit() {

    }
   getMoviesByName(formData: FormData){
   
        return this.httpClient.post('/movies/getByMoviename', formData);
   }

   getMoviesByCollection(formData: FormData){
   
    return this.httpClient.post('/movies/getByMovieCollection', formData);
}

getMoviesById(formData: FormData){
   
  return this.httpClient.post('/movies/getByMovieId', formData);
}
}
