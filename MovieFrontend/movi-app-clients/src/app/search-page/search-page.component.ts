import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchPageService } from './search-page.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent {
  constructor(private router:Router, private route: ActivatedRoute, private searchPageService: SearchPageService){}
  searchId:number = 0;
  searchStatus: boolean = true;
  movies: { movie_id: number, movie_name: string, movie_collection: string }[] = [];
  tableShow: boolean = false;
  ngOnInit(){
    this.searchId = this.route.snapshot.params['id'];
    console.log("SEARCH ID"+this.searchId);
  }
 
  submitSearch(submitForm: any){
    const formData = new FormData();
    formData.append('movie', submitForm.name);
    this.searchPageService.getMoviesByName(formData)
    .subscribe((response:any)=>{
      const resp = response;
      if(resp.length == 0){
        this.searchStatus = false;
      }else{
        this.tableShow = true;
        this.movies = resp;
      }
    }
    )
    console.log("Name>>>>>>"+submitForm.name);
  }

  submitSearchMovieCollection(submitForm: any){
    const formData = new FormData();
    formData.append('from', submitForm.from);
    formData.append('to', submitForm.to);
    this.searchPageService.getMoviesByCollection(formData)
    .subscribe((response:any)=>{
      console.log(response)
      const resp = response;
      if(resp.length == 0){
        this.searchStatus = false;
      }else{
        this.tableShow = true;
        this.movies = resp;
      }
    }
    )
  }

  home(){
    this.router.navigate(['']);
  }
  submitSearchbyId(submitForm: any){
    const formData = new FormData();
    formData.append('movieId', submitForm.movieId);
    this.searchPageService.getMoviesById(formData)
    .subscribe((response:any)=>{
      const resp = response;
      if(resp.length == 0){
        this.searchStatus = false;
      }else{
        this.tableShow = true;
        this.movies = resp;
      }
    }
    )
    console.log("Name>>>>>>"+submitForm.name);
  }
}
