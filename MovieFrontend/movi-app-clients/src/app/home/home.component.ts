import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
constructor(private router: Router){}
  login(){
    this.router.navigate(['/admin-login']);
  }

  byMovieId(){
    console.log("SEARCH 1");
    this.router.navigate(['/search/1']);

  }

  byMovieName(){
    console.log("SEARCH 2");
    this.router.navigate(['/search/2']);
  }

  byMovieCollection(){
    console.log("SEARCH 3");
    this.router.navigate(['/search/3']);
  }
}
